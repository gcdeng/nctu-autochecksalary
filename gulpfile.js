var gulp    = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var run = require('gulp-run')
var plugins = gulpLoadPlugins();


gulp.task('compile', () => {
  return gulp.src('./src/**/*.js')
  .pipe(plugins.plumber())
  .pipe(plugins.babel({
    presets: ['es2015', 'stage-0']
  }))
  .pipe(gulp.dest('dist'));
});

gulp.task('execute', ['compile'], () => {
  return run('node ./dist').exec()
})

gulp.task('watch', ['compile'], () => {

  gulp.watch('./src/**/*.js', ['execute']);

});
